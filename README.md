
# Table of Contents

1.  [Introduction](#orgb65dfb9)
    1.  [The QuantiSlakeTest approach (to be ammended)](#orgbd9ff34)
2.  [Installation](#org055794e)
    1.  [`nov. 2023 - PREFERRED SOLUTION` DEV version of slaker : Loading of the package from your computer](#org572a256)
        1.  [Download and extract the zip file on your computer](#org83f7507)
        2.  [Load the package using devtools](#org9a98d64)
    2.  [NOT WORKING NOW Installation of the package from gitlab in R](#org1180259)
3.  [Slaker Project](#org0ee3011)
    1.  [Create a blank project](#orgbab051b)
        1.  [Create folders](#orgc74aba2)
4.  [Slaking Lab](#orgc34916f)
    1.  [Scale properties](#orgccb530e)
    2.  [Equipment](#orgbd85d83)
        1.  [Geometric dimensioning and tolerancing](#org4c6d8c4)
        2.  [Details](#orge08ead2)
        3.  [Photos (CRA-W / D3-U07 / Bât. Petermann)](#org5e0e98a)
5.  [Usage](#orgc290072)
    1.  [Modus operandi of the slake test (in your labs) :](#orgf1d6894)
    2.  [Usage of the slaker package](#org3c1097a)
        1.  [Create the structure (here with examples)](#org0ba5f2d)
        2.  [Look at the structure and mimic it for your project](#orgdae3a82)
        3.  [Set working directory and some configuration variables](#org6ce0cd5)
        4.  [Run the application](#org6587379)
        5.  [Check up](#org27e7d9c)
        6.  [Notes](#orgd1ecafc)



<a id="orgb65dfb9"></a>

# Introduction

This R package provides functions for conducting a QuantiSlakeTest campaign, analysing and visualizing the results of a QuantiSlakeTest campaig. The easiest way to use it is through the ShinyApp included in the package.


<a id="orgbd9ff34"></a>

## The QuantiSlakeTest approach (to be ammended)

<p><iframe width="560" height="315" src="https://www.youtube.com/embed/G9UweThvHYI" frameborder="0" allowfullscreen></iframe></p>

[Link to the video of the experience](https://www.youtube.com/embed/5UfnbiBo-Ds?start=37).


<a id="org055794e"></a>

# Installation

slaker is on development and hosted on Gitlab.

The latest version of the R-packages, functions, tutorial and this Readme file is available here : <https://gitlab.com/FrdVnW/slaker>.

Using the package `devtools` <sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup> to install `slaker`, you can install it with the command : 

    install.packages("devtools")
    ## Maybe need also : 
    ## install.packages("stringi")
    ## install.packages("roxygen2")


<a id="org572a256"></a>

## `nov. 2023 - PREFERRED SOLUTION` DEV version of slaker : Loading of the package from your computer

[Temp. solution before a good update / document of new functions of the draft package]


<a id="org83f7507"></a>

### Download and extract the zip file on your computer

<https://gitlab.com/FrdVnW/slaker/-/archive/master/slaker-master.zip>

Save and extract on your computer and extract : ex, here in `~/Code/R/slaker/slaker/`. This path contain the present  `README.org` file.


<a id="org9a98d64"></a>

### Load the package using devtools

When `slaker` is on your computer, you can use this command to load it : 

    devtools::load_all("~/Code/R/slaker/slaker/")


<a id="org1180259"></a>

## TODO NOT WORKING NOW Installation of the package from gitlab in R

    devtools::install_git("https://gitlab.com/FrdVnW/slaker", upgrade_dependencies = FALSE)
    ## install_gitlab("FrdVnW/slaker", upgrade = "never") ## new version of devtools

This package is in a development state and is linked to work with the data collected in 
the framework of the PIRAT project of the CRA-W :

<http://www.cra.wallonie.be/fr/pirat>


<a id="org0ee3011"></a>

# Slaker Project


<a id="orgbab051b"></a>

## Create a blank project


<a id="orgc74aba2"></a>

### Create folders

    project_path <- "~/slaker-sample-project/"
    
    dir.create(project_path)
    
    for (dir in c("data-output","data-raw","fig","output")) {
      dir.create(paste0(project_path,dir))
    }
    
    list.files(project_path)


<a id="orgc34916f"></a>

# Slaking Lab


<a id="orgccb530e"></a>

## Scale properties

-   **max:** 1200 g (100cm³ sample, so often <200g ).
-   **d:** 0.01 g
-   **other features:** hook for "under scale" measure-balance. Connexion "Serial" vers USB (


<a id="orgbd85d83"></a>

## Equipment


<a id="org4c6d8c4"></a>

### Geometric dimensioning and tolerancing

![img](./man/figures/slaking_lab_cotes.png "Geometric dimensioning and tolerancing of the SlakingLab")


<a id="orge08ead2"></a>

### Details

![img](./man/figures/slaking_lab_poitions.png "Main positions of moving parts of the SlakingLab")


<a id="org5e0e98a"></a>

### Photos (CRA-W / D3-U07 / Bât. Petermann)

![img](./man/figures/slaking-lab-1.jpeg "Simple equipment with lab stands")

![img](./man/figures/slaking-lab-2.jpeg "Welded aluminium equipment")

![img](./man/figures/slaking-lab-cable.jpeg "Connection wire between each scale and computer")

![img](./man/figures/slaking-lab-computer.jpeg "Computer connections")

![img](./man/figures/slaking-lab-hook1.jpeg "Hook for suspended weighing 1")

![img](./man/figures/slaking-lab-hook2.jpeg "Hook for suspended weighing 2")


<a id="orgc290072"></a>

# Usage


<a id="orgf1d6894"></a>

## Modus operandi of the slake test (in your labs) :

1.  Launch the program (see 'Computer' section)
2.  Turn on the scale and check that it is level (bubble level)
3.  Put distilled water in the berlin (1400 ml for transparent berlin, up to the mark for opaque berlin)
4.  Position the empty basket suspended from the scale in the water
5.  Tare the scale
6.  Remove the berlin
7.  Place the sample in the basket
8.  Once the sample number is correctly established in the application click on 'Start the Slaketest'.
9.  When the graph begins to be constructed, quickly replace the basket with the sample in the water, making sure that the basket does not touch the edges
10. Once the slaketest is finished (when the slope is relatively nil, usually from 1000s), click on 'Stop the Slaketest'. Wait for the graph to disappear (this may take some iteration(s))
11. To limit the risk of bugs, it is better to switch the scales off and on again between each test.


<a id="org3c1097a"></a>

## Usage of the slaker package


<a id="org0ba5f2d"></a>

### Create the structure (here with examples)

Open R or RStudio in a well defined working directory on your computer. And create the project structure with the function `create_slaker_project`.

    slaker::create_slaker_project("projet_nico", sample=TRUE)

Here, example files are given in the folders.


<a id="orgdae3a82"></a>

### Look at the structure and mimic it for your project


<a id="org6ce0cd5"></a>

### Set working directory and some configuration variables

    setwd("./projet_nico/")
    
    ## Lignes de config
    fields.series <- c("campaign","serie","crop","parcel","bloc","modalite")
    fields.samples <- c("sample","campaign.samples","serie.samples","mode","profondeur","drying_temp","drying_j","eau")
    project_path <- paste0(getwd(),"/")


<a id="org6587379"></a>

### Run the application

    slaker::slaker()


<a id="org27e7d9c"></a>

### Check up

1.  First tab

    -   When the first tab is active, you have to see sample and campaings in a table

2.  Go to the 4th tab, the Visualizr and choose these options (!!!)

    You have an error message "object 'df.slake' not found" (OK)
    
    Then : 
    
    -   Select a file with your series (in ./data-raw) : *1-series-cyril.csv*
    -   Select a file with your samples (in ./data-raw) : *2-samples-new-cyril.csv*
    -   Select a file with your slakes (in ./data-raw) : *3-slakes-all.csv*
    -   Choose one or more campaing(s) for comparison : *CDM*
    -   Choose one drying protocol : *50*
    -   Choose some slakes for comparison : *[all are selected]*
    
    Then **Click to the above 'Update view' and &#x2026; wait (!!!)**. The error message has to dippear and you will see a first graph. 
    
    ![img](./man/figures/plotslakes.png)


<a id="orgd1ecafc"></a>

### Notes

\## subzone
\## in LTE - subzone = parcelle !

**WORK IN PROGRESS**


# Footnotes

<sup><a id="fn.1" href="#fnr.1">1</a></sup> if needed `install.packages(devtools)`
